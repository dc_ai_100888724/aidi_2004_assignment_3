from flask import Flask, redirect, render_template, request, jsonify, url_for
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
from datetime import datetime
#from models import Student

#create app instance
app = Flask(__name__)

#set database and initialize sqlachmy
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///students.db'
db = SQLAlchemy(app)

#initialize marshmallow
mar = Marshmallow(app)

#Student Model/Class
class Student(db.Model):
    student_id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50), nullable=False)
    last_name = db.Column(db.String(50), nullable=False)
    dob = db.Column(db.Date, nullable=False)
    amount_due = db.Column(db.Float, nullable=False)

def __repr__(self):
        return f"Student(id={self.id}, student_id='{self.student_id}', first_name='{self.first_name}', last_name='{self.last_name}', dob='{self.dob}', amount_due={self.amount_due})"

#Student Schema
class StudentSchema(mar.SQLAlchemyAutoSchema):
    class Meta:
        model = Student
        
#create database table
# Create the application context
with app.app_context():
    # Create the database tables
    db.create_all()

#ini

@app.route('/')
def index():
    students = Student.query.all()
    return render_template('index.html', students=students)

@app.route('/students', methods=['POST'])
def add_student():
    student_id = request.form['student_id']
    first_name = request.form['first_name']
    last_name = request.form['last_name']
    dob = request.form['dob']
    amount_due = request.form['amount_due']
    date_obj = datetime.strptime(dob, '%Y-%m-%d').date()
    new_student = Student(student_id=student_id, first_name=first_name, last_name=last_name, dob=date_obj, amount_due=amount_due)
    db.session.add(new_student)
    db.session.commit()
    return redirect(url_for('index'))

@app.route('/students/<int:id>', methods=['POST'])
def update_student(id):
    student = Student.query.get_or_404(id)
    student.first_name = request.form['first_name']
    student.last_name = request.form['last_name']
    student.dob = datetime.strptime(request.form['dob'], '%Y-%m-%d').date()
    student.amount_due = request.form['amount_due']
    db.session.commit()
    return redirect(url_for('index'))

@app.route('/students/<int:id>', methods=['GET'])
def edit_student(id):
    student = Student.query.get_or_404(id)
    return render_template('index.html', student=student)

@app.route('/students/<int:id>', methods=['DELETE'])
def delete_student(id):
    print("hello")
    student = Student.query.get(id)
    if not student:
        return jsonify({'message': 'Student not found'}), 404
    else:
        db.session.delete(student)
        db.session.commit()
        return jsonify({'message':'student deleted'}),200
    #return redirect(url_for('index'))
    #else:
    #    return jsonify({'message': 'Example not found'}), 404
    

if __name__ == '__main__':
    app.run(debug=True)
